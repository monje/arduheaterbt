ArduheaterBT
============

Please find last binary package for this app [here](https://gitlab.com/monje/arduheaterlite/-/tree/master/app)


About
-----

"ArduheaterBT" is an Android application that allow you to analise, show and
set parameters of the ["Arduheater" project](https://github.com/jbrazio/arduheater),
by jbrazio.

"Arduheater" is a full open source intelligent heat strip controller for
astronomy usage, based on Arduino.

"ArduheaterBT" connect with the arduheater strip controller by use a bluetooth
adapter, such as HC-06. See notes for details.

"ArduheaterBT" is part of the ["ArduheterLite" project](https://gitlab.com/monje/arduheaterlite),
which is a rebuild of the original "Arduheater" project adapted for be
"visual astronomy friendly". It is conceived for use in "auto" mode, with no
computer or screen needed. However, you must previously find the configuration
parameters for your particular setup. Find these parameter would be the task of
the application "ArduheterBT".

"ArduheaterBT" is released under MIT licence and you can
[find the code here](https://gitlab.com/monje/arduheaterbt)

Notes
-----

### General considerations
      
   * You can use a HC-06 bluetooth adapter for communicate with the "Arduheater"
     controller. You MUST configure it for a speed of 57600. Please refer to this
     [documentation](https://www.instructables.com/AT-command-mode-of-HC-05-Bluetooth-module/)
     for more info.

   * You MUST paired the bluetooth device to you Android system in order to be listed
     by the application as a valid bluetooth device.

### Issues and bugs

   * Saving the changed parameters in the Arduino eeprom seems does not works.
       
   * The read resistor values can be set negative due a bad conversion to string
     by "Arduheater". In this case, "ArduheaterBT" make a fix.

