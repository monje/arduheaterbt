package android.example.arduheaterbt

import android.example.arduheaterbt.databinding.ActivityShowSetBinding
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.pow

class ShowSetActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShowSetBinding

    private val adapter by lazy { PagerAdapterShowSet(this) }

    private val D_STATUS_POSTEMP_OFF: Int = 0
    private val D_STATUS_POSSET_OFF: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_show_set)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_set)
        binding.heaterdata = HeaterData
        binding.main = this

        //////Pager for fragment and tabs
        val pager: ViewPager2 = findViewById(R.id.pager)
        pager.adapter=adapter
        /// TAB.:
        // Inspect code: "Redundant SAM-constructor"
        //val tabLayoutMediator = TabLayoutMediator(binding.tablayout, pager,
        //        TabLayoutMediator.TabConfigurationStrategy { tab, position ->
        val tabLayoutMediator = TabLayoutMediator(binding.tablayout, pager
        ) { tab, position ->
            // In general, you can use here a when switch for the value of getInt("ARGUMENT")
            // but in this case is much easier
            /*
            when(position) {
                0 -> {
                    tab.text = "CHANNEL 1"
                }
                1 -> {
                    tab.text = "CHANNEL 2"
                }
                2 -> {
                    tab.text = "CHANNEL 3"
                }
                3 -> {
                    tab.text = "CHANNEL 4"
                }
            }
            */
            tab.text = "OUT ${position + 1}"
        }
        tabLayoutMediator.attach()
    }

    fun onClickSetAmbientTempOffset(){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(this, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = -10.0f
        val maxi = 10.0f
        val messageValue = "ambient temperature offset"
        val messageRange = "Signed decimal between $mini and $maxi"
        val mfilter = RangeFilter(-10.0f, 10.0f)
        setGDialog(messageValue, messageRange, D_STATUS_POSTEMP_OFF, (InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_SIGNED + InputType.TYPE_NUMBER_FLAG_DECIMAL), mfilter, 1)
    }

    fun onClickSetAmbientPointOffset(){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(this, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = -10.0f
        val maxi = 10.0f
        val messageValue = "ambient set point offset"
        val messageRange = "Signed decimal between $mini and $maxi"
        val mfilter = RangeFilter(mini, maxi)
        setGDialog(messageValue, messageRange, D_STATUS_POSSET_OFF, (InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_SIGNED + InputType.TYPE_NUMBER_FLAG_DECIMAL), mfilter, 1)
    }

    private fun changeGstring(position: Int, newvalue: String){
        var strin = ""
        var strout = ""

        strin= HeaterData.sag_status_G

        val tokens: MutableList<String> = strin.split(',').toMutableList()
        tokens[position] = newvalue
        strout = tokens.joinToString(",")

        HeaterData.sag_status_G = strout
    }

    private fun setGDialog(messageValue: String, messageRange: String, position: Int, type: Int, filter: RangeFilter, decimals: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Set $messageValue:")
        builder.setMessage(messageRange)

        val inputForm = EditText(this)
        inputForm.inputType = type
        inputForm.filters = arrayOf<InputFilter>(filter)
        builder.setView(inputForm)

        builder.setPositiveButton("SET") { _, _ ->
            if(inputForm.text.isEmpty()) return@setPositiveButton
            var newValue = 0
            val scale: Float = 10.0f.pow(decimals)
            val rawValuef: Float = inputForm.text.toString().toFloat()*scale
            //Inspect code: "Assignment should be lifted out of if"
            newValue = if(rawValuef < 0.0f) {
                ceil(rawValuef).toInt()
            } else {
                floor(rawValuef).toInt()
            }

            HeaterData.markForSetG = true
            changeGstring(position, newValue.toString())
            HeaterData.notifyMarkforsetd()
            Toast.makeText(this, "COMMAND SENT. PLEASE WAIT.", Toast.LENGTH_SHORT).show()
        }

        builder.setNegativeButton("CANCEL") { _, _ ->
            //Toast.makeText(this, "CANCEL PRESET.", Toast.LENGTH_SHORT).show()
        }

        /*builder.setNeutralButton("Maybe") { dialog, which ->
        Toast.makeText(applicationContext, "Maybe", Toast.LENGTH_SHORT).show()
        }*/
        builder.show()
    }
}