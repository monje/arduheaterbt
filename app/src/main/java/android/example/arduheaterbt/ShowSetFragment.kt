package android.example.arduheaterbt

import android.app.AlertDialog
import android.example.arduheaterbt.databinding.FragmentShowSetBinding
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.pow

class ShowSetFragment : Fragment() {

    private lateinit var binding: FragmentShowSetBinding

    private val D_STATUS_POSMIN_OUT: Int = 0
    private val D_STATUS_POSMAX_OUT: Int = 1
    private val D_STATUS_POSAUTO_START: Int = 2
    private val D_STATUS_POSTEMP_OFF: Int = 3
    private val D_STATUS_POSSET_OFF: Int = 4
    private val D_STATUS_POSKP: Int = 5
    private val D_STATUS_POSKI: Int = 6
    private val D_STATUS_POSKD: Int = 7

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_show_set, container, false)
        binding = FragmentShowSetBinding.inflate(inflater, container, false)
        binding.heaterdata = HeaterData
        binding.main = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey("ARGUMENT") }?.apply {
            binding.channel = getInt("ARGUMENT")
        }
    }

    fun onClickEnableDisable(channel: Int) {
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        if(HeaterData.oo_isenabled[channel]){
            HeaterData.markForDisable[channel] = true
            HeaterData.notifyMarkfordisableVar()
        }
        else{
            HeaterData.markForEnable[channel] = true
            HeaterData.notifyMarkforenableVar()
        }
        Toast.makeText(context, "COMMAND SENT. PLEASE WAIT.", Toast.LENGTH_SHORT).show()
    }

    fun onClickAutoStart(channel: Int) {
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        HeaterData.markForSetD[channel] = true
        HeaterData.notifyMarkforsetd()
        // Inspect code: "Simplify boolean expression"
        //if(HeaterData.sag_isAutoStart[channel] == false)  changeDstring(channel, 2, "1")
        if(!HeaterData.sag_isAutoStart[channel])  changeDstring(channel, 2, "1")
        else changeDstring(channel, D_STATUS_POSAUTO_START, "0")
        Toast.makeText(context, "COMMAND SENT. PLEASE WAIT.", Toast.LENGTH_SHORT).show()
    }

    /* TYPES:
    TYPE_NUMBER_FLAG_SIGNED
    TYPE_NUMBER_FLAG_DECIMAL
    TYPE_CLASS_NUMBER
     */
    fun onClickSetTempOffset(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = -10.0f
        val maxi = 10.0f
        val messageValue = "channel temperature offset"
        val messageRange = "Signed decimal between $mini and $maxi"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSTEMP_OFF, (InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_SIGNED + InputType.TYPE_NUMBER_FLAG_DECIMAL), mfilter, 1)
    }

    fun onClickSetPointOffset(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = -10.0f
        val maxi = 10.0f
        val messageValue = "channel set point offset"
        val messageRange = "Signed decimal between $mini and $maxi"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSSET_OFF, (InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_SIGNED + InputType.TYPE_NUMBER_FLAG_DECIMAL), mfilter, 1)
    }

    fun onClickSetMaxOut(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = 0.0f
        val maxi = 255.0f
        val messageValue = "maximum output"
        val messageRange = "Number between ${mini.toInt()} and ${maxi.toInt()}"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSMAX_OUT, (InputType.TYPE_CLASS_NUMBER), mfilter, 0)
    }

    fun onClickSetMinOut(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = 0.0f
        val maxi = 255.0f
        val messageValue = "minimum output"
        val messageRange = "Number between ${mini.toInt()} and ${maxi.toInt()}"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSMIN_OUT, (InputType.TYPE_CLASS_NUMBER), mfilter, 0)
    }

    fun onClickSetKp(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = 0.0f
        val maxi = 1000.0f
        val messageValue = "Kp"
        val messageRange = "Number between ${mini.toInt()} and ${maxi.toInt()}"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSKP, (InputType.TYPE_CLASS_NUMBER), mfilter, 0)
    }

    fun onClickSetKi(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = 0.0f
        val maxi = 100.0f
        val messageValue = "Ki"
        val messageRange = "Number between ${mini.toInt()} and ${maxi.toInt()}"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSKI, (InputType.TYPE_CLASS_NUMBER), mfilter, 0)
    }

    fun onClickSetKd(channel: Int){
        /*if(HeaterData.device.isNullOrEmpty()){
            Toast.makeText(context, "NO CONNECTED DEVICE.", Toast.LENGTH_LONG).show()
            return
        }** REDUCED */
        val mini = 0.0f
        val maxi = 10.0f
        val messageValue = "Kd"
        val messageRange = "Number between ${mini.toInt()} and ${maxi.toInt()}"
        val mfilter = RangeFilter(mini, maxi)
        setDDialog(messageValue, messageRange, channel, D_STATUS_POSKD, (InputType.TYPE_CLASS_NUMBER), mfilter, 0)
    }

    private fun changeDstring(channel: Int, position: Int, newvalue: String){
        var strin = ""
        var strout = ""
        when(channel){
            0 -> {
                strin= HeaterData.sag_status_D0 }
            1 -> {
                strin = HeaterData.sag_status_D1 }
            2 -> {
                strin = HeaterData.sag_status_D2 }
            3 -> {
                strin = HeaterData.sag_status_D3 }
        }

        val tokens: MutableList<String> = strin.split(',').toMutableList()
        tokens[position] = newvalue
        strout = tokens.joinToString(",")

        when(channel){
            0 -> {
                HeaterData.sag_status_D0 = strout }
            1 -> {
                HeaterData.sag_status_D1 = strout }
            2 -> {
                HeaterData.sag_status_D2 = strout }
            3 -> {
                HeaterData.sag_status_D3 = strout }
        }
    }

    private fun setDDialog(messageValue: String, messageRange: String, channel: Int, position: Int, type: Int, filter: RangeFilter, decimals: Int) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Set $messageValue:")
        builder.setMessage(messageRange)

        val inputForm = EditText(context)
        inputForm.inputType = type
        inputForm.filters = arrayOf<InputFilter>(filter)
        builder.setView(inputForm)

        builder.setPositiveButton("SET") { _, _ ->
            if(inputForm.text.isEmpty()) return@setPositiveButton
            var newValue = 0
            val scale: Float = 10.0f.pow(decimals)
            val rawValuef: Float = inputForm.text.toString().toFloat()*scale
            //Inspect code: "Assignment should be lifted out of if"
            newValue = if(rawValuef < 0.0f) {
                ceil(rawValuef).toInt()
            } else {
                floor(rawValuef).toInt()
            }

            HeaterData.markForSetD[channel] = true
            changeDstring(channel, position, newValue.toString())
            HeaterData.notifyMarkforsetd()
            Toast.makeText(context, "COMMAND SENT. PLEASE WAIT.", Toast.LENGTH_SHORT).show()
        }

        builder.setNegativeButton("CANCEL") { _, _ ->
            //Toast.makeText(context, "CANCEL PRESET.", Toast.LENGTH_SHORT).show()
        }

        /*builder.setNeutralButton("Maybe") { dialog, which ->
        Toast.makeText(applicationContext, "Maybe", Toast.LENGTH_SHORT).show()
        }*/
        builder.show()
    }

}
