package android.example.arduheaterbt

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

// I use singleton for share the data between MainActivity/activity_main layout and the service
object HeaterData : BaseObservable() {

    const val DEFAULT_CONNECT_TEXT = "CONNECT..."
    var CONNECT_TEXT = DEFAULT_CONNECT_TEXT
    /* Inspect code: Do not place Android context classes in static fields
    * I DON'T KNOW HOW TO FIX THIS INSPECT CODE WARING */
    var menuActivity: MainActivity? = null

    var version: String = ""

    // STATUS FLAGS
    var isFirstTime: Boolean = true

    @get:Bindable
    var markForEnable: BooleanArray = booleanArrayOf(false, false, false, false)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.markForEnable)
        }

    @get:Bindable
    var markForDisable: BooleanArray = booleanArrayOf(false, false, false, false)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.markForDisable)
        }

    @get:Bindable
    var markForSetD: BooleanArray = booleanArrayOf(false, false, false, false)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.markForSetD)
        }

    @get:Bindable
    var markForSetG: Boolean = false
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.markForSetG)
        }

    /////////////////////////////////
    /// BINDABLE VARIABLES

    @get:Bindable
    var device : String = ""
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.device)
        }

    @get:Bindable
    var oo_isconnected: BooleanArray = booleanArrayOf(false, false, false, false)//BooleanArray(4)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.oo_isconnected)
        }

    @get:Bindable
    var oo_isready: BooleanArray = booleanArrayOf(false, false, false, false)//BooleanArray(4)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.oo_isready)
        }

    @get:Bindable
    var oo_isenabled: BooleanArray = booleanArrayOf(false, false, false, false)//BooleanArray(4)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.oo_isenabled)
        }

    @get:Bindable
    var oo_ambientTemp: String = ""
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.oo_ambientTemp)
        }

    @get:Bindable
    var oo_ambientHumi: String = ""
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.oo_ambientHumi)
        }

    @get:Bindable
    var oo_ambientDew: String = ""
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.oo_ambientDew)
        }

    @get:Bindable
    var os_nominalTemp: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.os_nominalTemp)
        }

    @get:Bindable
    var os_resistorVal: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.os_resistorVal)
        }

    @get:Bindable
    var os_bValue: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.os_bValue)
        }

    @get:Bindable
    var os_nominalVal: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.os_nominalVal)
        }

    @get:Bindable
    var ifenabled_temperature: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.ifenabled_temperature)
        }

    @get:Bindable
    var ifenabled_setpoint: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.ifenabled_setpoint)
        }

    @get:Bindable
    var ifenabled_outvalue: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.ifenabled_outvalue)
        }

    @get:Bindable
    var sag_minOut: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_minOut)
        }

    @get:Bindable
    var sag_maxOut: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_maxOut)
        }

    @get:Bindable
    var sag_isAutoStart: BooleanArray = booleanArrayOf(false, false, false, false)
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_isAutoStart)
        }

    @get:Bindable
    var sag_tempOffset: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_tempOffset)
        }

    @get:Bindable
    var sag_setpointOffset: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_setpointOffset)
        }

    @get:Bindable
    var sag_Kp: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_Kp)
        }

    @get:Bindable
    var sag_Ki: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_Ki)
        }

    @get:Bindable
    var sag_Kd: MutableList<String> = mutableListOf("", "", "", "")
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_Kd)
        }

    @get:Bindable
    var sag_ambientTempOffset: String = ""
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_ambientTempOffset)
        }

    @get:Bindable
    var sag_ambientRhOffset: String = ""
        // Redundant getter
        //get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sag_ambientRhOffset)
        }

    //////////////////////////////////////////////////////////////////////
    // Vars. Just for store raw returns of commands and set value variable
    ///////////////////////////////////////////////////////////////////////////

    // OUTPUT ONLY COMMANDS
    var oo_status_q: String =""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            for (i in 0..3){
                this.oo_isconnected[i] = (data[i] == "1")
                this.oo_isready[i] = (data[i+4] == "1")
                this.oo_isenabled[i] = (data[i+8] == "1")
            }
            notifyPropertyChanged(BR.oo_isconnected)
            notifyPropertyChanged(BR.oo_isready)
            notifyPropertyChanged(BR.oo_isenabled)
        }

    var oo_status_A: String =""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.oo_ambientTemp = decimal(data[0])
            this.oo_ambientHumi = decimal(data[1])
            this.oo_ambientDew = decimal(data[2])
        }

    //////// ONESHOT COMMAND
    var os_status_F0: String = ""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.os_nominalTemp[0] = data[0]
            this.os_resistorVal[0] = checkUnsigned(data[1])
            this.os_bValue[0] = data[2]
            this.os_nominalVal[0] = data[3]
            notifyPropertyChanged(BR.os_nominalTemp)
            notifyPropertyChanged(BR.os_resistorVal)
            notifyPropertyChanged(BR.os_bValue)
            notifyPropertyChanged(BR.os_nominalVal)
        }

    var os_status_F1: String = ""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.os_nominalTemp[1] = data[0]
            this.os_resistorVal[1] = checkUnsigned(data[1])
            this.os_bValue[1] = data[2]
            this.os_nominalVal[1] = data[3]
            notifyPropertyChanged(BR.os_nominalTemp)
            notifyPropertyChanged(BR.os_resistorVal)
            notifyPropertyChanged(BR.os_bValue)
            notifyPropertyChanged(BR.os_nominalVal)
        }

    var os_status_F2: String = ""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.os_nominalTemp[2] = data[0]
            this.os_resistorVal[2] = checkUnsigned(data[1])
            this.os_bValue[2] = data[2]
            this.os_nominalVal[2] = data[3]
            notifyPropertyChanged(BR.os_nominalTemp)
            notifyPropertyChanged(BR.os_resistorVal)
            notifyPropertyChanged(BR.os_bValue)
            notifyPropertyChanged(BR.os_nominalVal)
        }

    var os_status_F3: String = ""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.os_nominalTemp[3] = data[0]
            this.os_resistorVal[3] = checkUnsigned(data[1])
            this.os_bValue[3] = data[2]
            this.os_nominalVal[3] = data[3]
            notifyPropertyChanged(BR.os_nominalTemp)
            notifyPropertyChanged(BR.os_resistorVal)
            notifyPropertyChanged(BR.os_bValue)
            notifyPropertyChanged(BR.os_nominalVal)
        }

    // QUERY ONLY IF ENABLED COMMANDS
    var ifenabled_status_B0: String =""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.ifenabled_temperature[0] = decimal(data[0])
            this.ifenabled_setpoint[0] = decimal(data[1])
            this.ifenabled_outvalue[0] = data[2]
            notifyPropertyChanged(BR.ifenabled_temperature)
            notifyPropertyChanged(BR.ifenabled_setpoint)
            notifyPropertyChanged(BR.ifenabled_outvalue)
        }

    var ifenabled_status_B1: String =""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.ifenabled_temperature[1] = decimal(data[0])
            this.ifenabled_setpoint[1] = decimal(data[1])
            this.ifenabled_outvalue[1] = data[2]
            notifyPropertyChanged(BR.ifenabled_temperature)
            notifyPropertyChanged(BR.ifenabled_setpoint)
            notifyPropertyChanged(BR.ifenabled_outvalue)
        }

    var ifenabled_status_B2: String =""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.ifenabled_temperature[2] = decimal(data[0])
            this.ifenabled_setpoint[2] = decimal(data[1])
            this.ifenabled_outvalue[2] = data[2]
            notifyPropertyChanged(BR.ifenabled_temperature)
            notifyPropertyChanged(BR.ifenabled_setpoint)
            notifyPropertyChanged(BR.ifenabled_outvalue)
        }

    var ifenabled_status_B3: String =""
        set(value) {
            field = value
            val data: List<String> = value.split(',')
            this.ifenabled_temperature[3] = decimal(data[0])
            this.ifenabled_setpoint[3] = decimal(data[1])
            this.ifenabled_outvalue[3] = data[2]
            notifyPropertyChanged(BR.ifenabled_temperature)
            notifyPropertyChanged(BR.ifenabled_setpoint)
            notifyPropertyChanged(BR.ifenabled_outvalue)
        }

    // SET AND GET COMMANDS
    var sag_status_D0: String =""
        set(value) {
            field = value
            if(!this.markForSetD[0]) {
                val data: List<String> = value.split(',')
                this.sag_minOut[0] = data[0]
                this.sag_maxOut[0] = data[1]
                this.sag_isAutoStart[0] = (data[2] == "1")
                this.sag_tempOffset[0] = decimal(data[3])
                this.sag_setpointOffset[0] = decimal(data[4])
                this.sag_Kp[0] = data[5]
                this.sag_Ki[0] = data[6]
                this.sag_Kd[0] = data[7]
                notifyPropertyChanged(BR.sag_minOut)
                notifyPropertyChanged(BR.sag_maxOut)
                notifyPropertyChanged(BR.sag_isAutoStart)
                notifyPropertyChanged(BR.sag_tempOffset)
                notifyPropertyChanged(BR.sag_setpointOffset)
                notifyPropertyChanged(BR.sag_Kp)
                notifyPropertyChanged(BR.sag_Ki)
                notifyPropertyChanged(BR.sag_Kd)
            }
        }

    var sag_status_D1: String =""
        set(value) {
            field = value
            if(!this.markForSetD[1]) {
                val data: List<String> = value.split(',')
                this.sag_minOut[1] = data[0]
                this.sag_maxOut[1] = data[1]
                this.sag_isAutoStart[1] = (data[2] == "1")
                this.sag_tempOffset[1] = decimal(data[3])
                this.sag_setpointOffset[1] = decimal(data[4])
                this.sag_Kp[1] = data[5]
                this.sag_Ki[1] = data[6]
                this.sag_Kd[1] = data[7]
                notifyPropertyChanged(BR.sag_minOut)
                notifyPropertyChanged(BR.sag_maxOut)
                notifyPropertyChanged(BR.sag_isAutoStart)
                notifyPropertyChanged(BR.sag_tempOffset)
                notifyPropertyChanged(BR.sag_setpointOffset)
                notifyPropertyChanged(BR.sag_Kp)
                notifyPropertyChanged(BR.sag_Ki)
                notifyPropertyChanged(BR.sag_Kd)
            }
        }

    var sag_status_D2: String =""
        set(value) {
            field = value
            if(!this.markForSetD[2]) {
                val data: List<String> = value.split(',')
                this.sag_minOut[2] = data[0]
                this.sag_maxOut[2] = data[1]
                this.sag_isAutoStart[2] = (data[2] == "1")
                this.sag_tempOffset[2] = decimal(data[3])
                this.sag_setpointOffset[2] = decimal(data[4])
                this.sag_Kp[2] = data[5]
                this.sag_Ki[2] = data[6]
                this.sag_Kd[2] = data[7]
                notifyPropertyChanged(BR.sag_minOut)
                notifyPropertyChanged(BR.sag_maxOut)
                notifyPropertyChanged(BR.sag_isAutoStart)
                notifyPropertyChanged(BR.sag_tempOffset)
                notifyPropertyChanged(BR.sag_setpointOffset)
                notifyPropertyChanged(BR.sag_Kp)
                notifyPropertyChanged(BR.sag_Ki)
                notifyPropertyChanged(BR.sag_Kd)
            }
        }

    var sag_status_D3: String =""
        set(value) {
            field = value
            if(!this.markForSetD[3]) {
                val data: List<String> = value.split(',')
                this.sag_minOut[3] = data[0]
                this.sag_maxOut[3] = data[1]
                this.sag_isAutoStart[3] = (data[2] == "1")
                this.sag_tempOffset[3] = decimal(data[3])
                this.sag_setpointOffset[3] = decimal(data[4])
                this.sag_Kp[3] = data[5]
                this.sag_Ki[3] = data[6]
                this.sag_Kd[3] = data[7]
                notifyPropertyChanged(BR.sag_minOut)
                notifyPropertyChanged(BR.sag_maxOut)
                notifyPropertyChanged(BR.sag_isAutoStart)
                notifyPropertyChanged(BR.sag_tempOffset)
                notifyPropertyChanged(BR.sag_setpointOffset)
                notifyPropertyChanged(BR.sag_Kp)
                notifyPropertyChanged(BR.sag_Ki)
                notifyPropertyChanged(BR.sag_Kd)
            }
        }

    var sag_status_G: String =""
        set(value) {
            field = value
            if(!this.markForSetG) {
                val data: List<String> = value.split(',')
                this.sag_ambientTempOffset = decimal(data[0])
                this.sag_ambientRhOffset = decimal(data[1])
            }
        }

/////////////////////////////////

    private fun decimal(input: String): String{
        var result: String = (input.substring(0, input.length-1) + "." + input.substring(input.length-1, input.length))
        //if(result[0] == '.') result = "0$result"
        if(result.startsWith(".")) result = "0$result"
        else if(result.startsWith("-.")) {
            result = "-0.${result.substringAfter("-.")}"
        }
        return result
    }

    fun notifyIfenabledVars(){
        notifyPropertyChanged(BR.ifenabled_temperature)
        notifyPropertyChanged(BR.ifenabled_setpoint)
        notifyPropertyChanged(BR.ifenabled_outvalue)
    }

    fun notifyMarkforenableVar(){
        notifyPropertyChanged(BR.markForEnable)
    }

    fun notifyMarkfordisableVar(){
        notifyPropertyChanged(BR.markForDisable)
    }

    fun notifyMarkforsetd(){
        notifyPropertyChanged(BR.markForSetD)
    }

    private fun checkUnsigned(input: String): String{
        /*
        Assuming the input is 16 bits long, if it is negative
        the unsigned correspondent value in the 2 complement
        representation will be:
        unsigned = (2**16) + negative = 65536 + negative
         */
        var outputValue = 0L
        val addValue = 65536L
        val inputValue = input.toLong()

        //Inspect code: "Assignment should be lifted out of if"
        //if(inputValue < 0L){
        //    outputValue = inputValue + addValue
        //}
        //else outputValue = inputValue

        outputValue = if(inputValue < 0L){
            inputValue + addValue
        } else inputValue

        return outputValue.toString()
    }

}