package android.example.arduheaterbt

import android.app.Service
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.widget.Toast
import com.github.mikephil.charting.data.Entry
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.util.*
import kotlin.text.Charsets.US_ASCII

class ArduheaterbtService : Service() {

    private lateinit var myDevice: BluetoothDevice
    private lateinit var btSocket: BluetoothSocket
    private lateinit var writer: OutputStreamWriter
    private lateinit var reader: InputStreamReader

    private lateinit var mainThread: Thread
    private lateinit var mHandler: Handler

    //default delay value 5000 ms.
    // declared as var due may be value could be fixed from outside
    private var delay: Long = 5000
    private val BUFFER_SIZE: Int = 64

    // Be sure that length of TEST_ARDUHEATER is shorter that BUFFER_SIZE
    private val TEST_ARDUHEATER = "Currently there is no support for a human on the serial line."

    override fun onCreate() {
        super.onCreate()
        HeaterData.isFirstTime = true
        HeaterData.markForEnable = booleanArrayOf(false, false, false, false)
        HeaterData.markForDisable = booleanArrayOf(false, false, false, false)
        HeaterData.markForSetD = booleanArrayOf(false, false, false, false)
        HeaterData.markForSetG = false

        mHandler = Handler(Looper.getMainLooper())
        mainThread = Thread(mainRunnable)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        getMydevice(intent)
        doConnect()
        if (btSocket.isConnected) {
            if (testArduheater()) {
                //val out = query("V")
                HeaterData.version = query("V")
                HeaterData.CONNECT_TEXT = "DISCONNECT"
                Toast.makeText(applicationContext, "CONNECTED TO ARDUHEATER ${HeaterData.version}", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "NO COMPATIBLE DEVICE", Toast.LENGTH_LONG).show()
                stopSelf()
            }
        }
        mainThread.start()
        /*super.onStartCommand(intent, flags, startId)*/
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        //super.onDestroy()
        doDisconnect()
        for(i in 0..3){
            if (!HeaterData.oo_isconnected[i] || !HeaterData.oo_isenabled[i]) continue
            PlotData.pauseMark(i)
        }
        resetValues()
    }

    override fun onBind(intent: Intent): IBinder? {
        // According documentation, if not used, this must return null
        return null
    }

    //// PRIVATE FUNCTIONS
    private fun getMydevice(intent: Intent?) {
        this.myDevice = intent!!.getParcelableExtra("myDev")!!
        HeaterData.device = myDevice.name
    }

    private fun doConnect() {
        //This UUID seems to be "standard" for this type of connection
        val myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        btSocket = myDevice.createInsecureRfcommSocketToServiceRecord(myUUID)
        HeaterData.device = myDevice.name

        try {
            btSocket.connect()
            reader = InputStreamReader(btSocket.inputStream, US_ASCII)
            writer = OutputStreamWriter(btSocket.outputStream, US_ASCII)
            HeaterData.CONNECT_TEXT = "DISCONNECT"
            HeaterData.menuActivity?.invalidateOptionsMenu()
        } catch (e: IOException) {
            Toast.makeText(applicationContext, "CAN'T CONNECT", Toast.LENGTH_LONG).show()
            stopSelf()
        }
    }

    private fun doDisconnect() {
        //if(mainThread.isAlive) mainThread.stop() //Thread.stop is deprecated
        if (btSocket.isConnected) {
            writer.close()
            reader.close()
            btSocket.close()
        }
        HeaterData.CONNECT_TEXT = HeaterData.DEFAULT_CONNECT_TEXT
        HeaterData.menuActivity?.invalidateOptionsMenu()
    }

    private var mainRunnable: Runnable = object : Runnable {
        override fun run() {
            if (btSocket.isConnected && testArduheater()){
            // option before works better
            //if (btSocket.isConnected) {
                doMainLoop()
                updatePlots()
                mHandler.postDelayed(this, delay)
            } else stopSelf()
        }
    }

    private fun doMainLoop() {
        if (btSocket.isConnected && testArduheater()){
        // option before works better
        //if (btSocket.isConnected) {
         if(!HeaterData.isFirstTime) {
             //DO MARKET FOR SETS
             for(i in 0..3){
                 if (HeaterData.markForEnable[i]) {
                     rawquery("+${i}", false); HeaterData.markForEnable[i] = false
                     HeaterData.notifyMarkforenableVar()
                 }
                 if (HeaterData.markForDisable[i]) {
                     rawquery("-${i}", false);HeaterData.markForDisable[i] = false
                     HeaterData.notifyMarkfordisableVar()
                     if(HeaterData.oo_isconnected[i]){PlotData.pauseMark(i)}
                     HeaterData.ifenabled_temperature[i]=""
                     HeaterData.ifenabled_setpoint[i]=""
                     HeaterData.ifenabled_outvalue[i]=""
                     HeaterData.notifyIfenabledVars()
                 }
             }
             // Unfortunately, the following if statements are not easily embeddable in a for,
             // given the specific command (not indexed) of the last line of each
             if (HeaterData.markForSetD[0]) {
                 rawquery("D0,${HeaterData.sag_status_D0}", false)
                 HeaterData.markForSetD[0] = false
                 HeaterData.notifyMarkforsetd()
                 sagCommandD0()
             }
             if (HeaterData.markForSetD[1]) {
                 rawquery("D1,${HeaterData.sag_status_D1}", false)
                 HeaterData.markForSetD[1] = false
                 HeaterData.notifyMarkforsetd()
                 sagCommandD1()
             }
             if (HeaterData.markForSetD[2]) {
                 rawquery("D2,${HeaterData.sag_status_D2}", false)
                 HeaterData.markForSetD[2] = false
                 HeaterData.notifyMarkforsetd()
                 sagCommandD2()
             }
             if (HeaterData.markForSetD[3]) {
                 rawquery("D3,${HeaterData.sag_status_D3}", false)
                 HeaterData.markForSetD[3] = false
                 HeaterData.notifyMarkforsetd()
                 sagCommandD3()
             }
             if (HeaterData.markForSetG) {
                 rawquery("G${HeaterData.sag_status_G}", false)
                 HeaterData.markForSetG = false
                 sagCommandG()
             }
         }

            //DO READS
            ooCommandQ()
            ooCommandA()
            if (HeaterData.isFirstTime) {
                osCommandF0()
                osCommandF1()
                osCommandF2()
                osCommandF3()
                sagCommandD0()
                sagCommandD1()
                sagCommandD2()
                sagCommandD3()
                sagCommandG()
            }
            if(HeaterData.oo_isenabled[0]) ifenabledCommandB0()
            if(HeaterData.oo_isenabled[1]) ifenabledCommandB1()
            if(HeaterData.oo_isenabled[2]) ifenabledCommandB2()
            if(HeaterData.oo_isenabled[3]) ifenabledCommandB3()

            HeaterData.isFirstTime = false
        }
    }

    private fun rawquery(command: String, getOutput: Boolean = true): CharArray {
        val mybuffer = CharArray(BUFFER_SIZE)
        if (btSocket.isConnected) {
            try {
                if(reader.ready()) reader.read(mybuffer) //poor man flush
                writer.write(command + "\n")
                writer.flush()

                if(getOutput){
                    reader.read(mybuffer)
                }
            } catch (e: IOException) {
                stopSelf()
            }
        }
        else stopSelf()
        //println("DEBUG --> ${mybuffer.toString()}")
        println("DEBUG --> $mybuffer")
        return mybuffer
    }

    private fun query(command: String): String{
        val tmp: CharArray = rawquery(command)
        //if command is character. Only is channel as parameter there is an extra ","
        //Inspect code: "Can be joined with assignment"
        //val tmpoffset: Int
        //Inspect code: "Assignment should be lifted out of if"
        //if (command.length == 1) tmpoffset = command.length+1
        val tmpoffset: Int = if (command.length == 1) command.length+1
        else command.length+2 //returned in form: :CC,.... => command length +2
        return String(tmp, tmpoffset, BUFFER_SIZE-tmpoffset).substringBefore('#')
    }

    private fun testArduheater(): Boolean {
        var tmp = false
        if (btSocket.isConnected) {
            val out = rawquery("$")
            tmp = String(out).startsWith(TEST_ARDUHEATER)
        }
        return tmp
    }

    // Reset data in disconnection
    private fun resetValues(){
        HeaterData.version = ""
        HeaterData.device = ""

        HeaterData.isFirstTime = true
        HeaterData.markForEnable = booleanArrayOf(false, false, false, false)
        HeaterData.markForDisable = booleanArrayOf(false, false, false, false)
        HeaterData.markForSetD = booleanArrayOf(false, false, false, false)
        HeaterData.markForSetG = false

        HeaterData.oo_isconnected = booleanArrayOf(false, false, false, false)
        HeaterData.oo_isready = booleanArrayOf(false, false, false, false)
        HeaterData.oo_isenabled = booleanArrayOf(false, false, false, false)
        HeaterData.oo_ambientTemp = ""
        HeaterData.oo_ambientHumi = ""
        HeaterData.oo_ambientDew = ""
        HeaterData.os_nominalTemp = mutableListOf("", "", "", "")
        HeaterData.os_resistorVal = mutableListOf("", "", "", "")
        HeaterData.os_bValue = mutableListOf("", "", "", "")
        HeaterData.os_nominalVal = mutableListOf("", "", "", "")
        HeaterData.ifenabled_temperature = mutableListOf("", "", "", "")
        HeaterData.ifenabled_setpoint = mutableListOf("", "", "", "")
        HeaterData.ifenabled_outvalue = mutableListOf("", "", "", "")
        HeaterData.sag_minOut = mutableListOf("", "", "", "")
        HeaterData.sag_maxOut = mutableListOf("", "", "", "")
        HeaterData.sag_isAutoStart = booleanArrayOf(false, false, false, false)
        HeaterData.sag_tempOffset = mutableListOf("", "", "", "")
        HeaterData.sag_setpointOffset = mutableListOf("", "", "", "")
        HeaterData.sag_Kp = mutableListOf("", "", "", "")
        HeaterData.sag_Ki = mutableListOf("", "", "", "")
        HeaterData.sag_Kd = mutableListOf("", "", "", "")
        HeaterData.sag_ambientTempOffset = ""
        HeaterData.sag_ambientRhOffset = ""
    }

    /////////////////////////////////////////////////////////////////////
    // functions for send every commands (except help and version)
    // set only commands:
    // +
    // -

    //////// OUTPUT ONLY COMMANDS
    private fun ooCommandQ(){
        HeaterData.oo_status_q =query("?")
    }

    private fun ooCommandA(){
        HeaterData.oo_status_A =query("A")
    }

    //////// ONESHOT COMMAND
    private fun osCommandF0(){
        HeaterData.os_status_F0 = query("F0")
    }

    private fun osCommandF1(){
        HeaterData.os_status_F1 = query("F1")
    }

    private fun osCommandF2(){
        HeaterData.os_status_F2 = query("F2")
    }

    private fun osCommandF3(){
        HeaterData.os_status_F3 = query("F3")
    }

    //////// QUERY ONLY IF ENABLED COMMANDS
    private fun ifenabledCommandB0(){
        HeaterData.ifenabled_status_B0 =query("B0")
    }

    private fun ifenabledCommandB1(){
        HeaterData.ifenabled_status_B1 =query("B1")
    }

    private fun ifenabledCommandB2(){
        HeaterData.ifenabled_status_B2 =query("B2")
    }

    private fun ifenabledCommandB3(){
        HeaterData.ifenabled_status_B3 =query("B3")
    }

    // SET AND GET COMMANDS
    private fun sagCommandD0(){
        HeaterData.sag_status_D0 =query("D0")
    }

    private fun sagCommandD1(){
        HeaterData.sag_status_D1 =query("D1")
    }

    private fun sagCommandD2(){
        HeaterData.sag_status_D2 =query("D2")
    }

    private fun sagCommandD3(){
        HeaterData.sag_status_D3 =query("D3")
    }

    private fun sagCommandG(){
        HeaterData.sag_status_G =query("G")
    }

    private fun updatePlots(){
        for(i in 0..3) {
            if (!HeaterData.oo_isconnected[i] || !HeaterData.oo_isenabled[i]) continue

            val nextX = PlotData.nextVal[i]

            PlotData.index_DewPoint[i]?.let {
                PlotData.plotChart[i]?.data?.addEntry(
                    Entry(nextX, HeaterData.oo_ambientDew.toFloat()),
                    it
                )
            }
            PlotData.index_SetPoint[i]?.let {
                PlotData.plotChart[i]?.data?.addEntry(
                    Entry(nextX, HeaterData.ifenabled_setpoint[i].toFloat()),
                    it
                )
            }
            PlotData.index_Temperature[i]?.let {
                PlotData.plotChart[i]?.data?.addEntry(
                    Entry(nextX, HeaterData.ifenabled_temperature[i].toFloat()),
                    it
                )
            }

            PlotData.nextVal[i] += (delay.toFloat()/1000f)
            PlotData.plotChart[i]?.notifyDataSetChanged()
            PlotData.plotChart[i]?.postInvalidate()
        }
    }
}
