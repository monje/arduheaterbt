package android.example.arduheaterbt

import android.app.AlertDialog
import android.content.pm.PackageManager
import android.example.arduheaterbt.databinding.FragmentHeaterBinding
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.Toast
import androidx.core.content.PermissionChecker.checkSelfPermission

class HeaterFragment : Fragment() {
    private val ACTIVITY_PERMISSION_REQUEST = 0

    private lateinit var binding: FragmentHeaterBinding

    // NOTE: I don't understand why, but in the docs _binding is used before define binding
    // SEE: https://developer.android.com/topic/libraries/view-binding
    //private var _binding: FragmentHeaterBinding? = null
    //private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //SEE NOTE BEFORE //binding = FragmentHeaterBinding.inflate(inflater)
        //SEE NOTE BEFORE //_binding = FragmentHeaterBinding.inflate(inflater)
        //SEE NOTE BEFORE //_binding = FragmentHeaterBinding.inflate(inflater, container, false)
        binding = FragmentHeaterBinding.inflate(inflater, container, false)
        binding.heaterdata = HeaterData
        binding.main = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //super.onViewCreated(view, savedInstanceState)

        arguments?.takeIf { it.containsKey("ARGUMENT") }?.apply {
            // In general, you can use here a when switch for the value of getInt("ARGUMENT")
            // but in this case is much easier
            val channel: Int = getInt("ARGUMENT")
            binding.channel = channel
            PlotData.plotContext[channel] = context
            if(PlotData.plotChart[channel] == null) PlotData.makePlot(channel)
        }
    }

    override fun onResume() {
        super.onResume()

        launchPlot(binding.channel)
    }

    override fun onPause() {
        super.onPause()
        
        val channel: Int = binding.channel
        binding.plotSlot.removeView(PlotData.plotChart[channel])
        
    }

    private fun launchPlot(channel: Int){
        val channel: Int = binding.channel
        binding.plotSlot.addView(PlotData.plotChart[channel])
        PlotData.plotChart[channel]?.layoutParams?.width  = MATCH_PARENT
        PlotData.plotChart[channel]?.layoutParams?.height  = MATCH_PARENT

        PlotData.plotChart[channel]?.setOnLongClickListener{plotDialog()}

        PlotData.plotChart[channel]?.invalidate()
    }

    private fun plotDialog(): Boolean {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Select action over plot:")
        /* Added to manifest android:requestLegacyExternalStorage="true" for WRITE_EXTERNAL_STORAGE compatibility in Android 10
        I think this will not works in android 11, but I does not know a way to address this (by the moment) */
        builder.setPositiveButton("SAVE") { _, _ ->
            val perm: Array<String> = Array(1){"android.permission.WRITE_EXTERNAL_STORAGE"}
            if (context?.let { checkSelfPermission(it, perm[0]) } == PackageManager.PERMISSION_DENIED){
                requestPermissions(perm, ACTIVITY_PERMISSION_REQUEST)
            }
            else {PlotData.savePlot(binding.channel)}
        }

        builder.setNegativeButton("CLEAR") { _, _ ->
            if(PlotData.nextVal[binding.channel] != 0f) {
                plotCleanDialog()
            }
            else{
                Toast.makeText(context, "PLOT WITHOUT DATA: CLEAN ABORTED", Toast.LENGTH_LONG).show()
            }
        }

        builder.setNeutralButton("CANCEL") { _, _ ->
        }

        builder.show()
        return true
    }

    private fun plotCleanDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Confirm Clear Plot:")
        builder.setMessage("Data will lose at clear plot")

        builder.setPositiveButton("CLEAR PLOT") { _, _ ->
            val channel: Int = binding.channel
            PlotData.clearPlot(channel)
            binding.plotSlot.removeView(PlotData.plotChart[channel])
            PlotData.plotChart[channel] = null
            PlotData.makePlot(channel)
            launchPlot(channel)
        }

        /*builder.setNegativeButton("CANCEL") { dialog, which ->
            Toast.makeText(applicationContext, "CANCEL PRESSED", Toast.LENGTH_SHORT).show()
        }*/

        // Cancel button at left, for coherence with other 3-choice dialogs
        builder.setNeutralButton("CANCEL") { dialog, which ->
        }
        builder.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode){
            ACTIVITY_PERMISSION_REQUEST -> {
                PlotData.savePlot(binding.channel)
            }
            else -> {
                Toast.makeText(context, "NO REQUEST CODE", Toast.LENGTH_LONG).show()
            }
        }

    }

}
