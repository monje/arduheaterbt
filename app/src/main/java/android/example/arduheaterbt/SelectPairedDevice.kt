package android.example.arduheaterbt

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class SelectPairedDevice : AppCompatActivity() {

    private lateinit var pairedDevices: Set<BluetoothDevice>

    private val REQUEST_ENABLE_BT = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_paired_device)

        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

        val listDevices: ListView = findViewById(R.id.listDevices)

        if (bluetoothAdapter?.isEnabled == true) {
            pairedDevices = bluetoothAdapter.bondedDevices
            val devNames = arrayOfNulls<String>(pairedDevices.size)
            var i = 0
            this.pairedDevices.forEach { device ->
                devNames[i] = device.name + "\n" + device.address
                i += 1
            }
            val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, devNames)
            listDevices.adapter = adapter
        } else {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        listDevices.setOnItemClickListener { _, _, position, _ ->
            val intent = Intent()
            intent.putExtra("myDev", this.pairedDevices.elementAt(position))
            setResult(Activity.RESULT_OK, intent)
            Toast.makeText(applicationContext, "TRYING TO CONNECT...", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            else -> {
                Toast.makeText(applicationContext, "NO REQUEST CODE", Toast.LENGTH_LONG).show()
            }
        }
    }
}