package android.example.arduheaterbt

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.widget.Toast
import androidx.core.content.PermissionChecker
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import java.text.SimpleDateFormat
import java.util.*

// singleton for plot stuff
object PlotData {
    // The four plot charts (one per channel)
    // Inspect code return "Redundant lambda arrow" here. I don't knw how to fix it...
    var plotChart: Array<LineChart?> = Array(4) { null }

    var index_DewPoint: Array<Int?> = Array(4) { null }
    var index_SetPoint: Array<Int?> = Array(4) { null }
    var index_Temperature: Array<Int?> = Array(4) { null }

    var nextVal: Array<Float> = Array(4) { 0f }

    private var timestamptInit: Array<String?> = Array(4) { null }

    var plotContext: Array<Context?> = Array(4) { null }

    fun makePlot(channel: Int) {

        val plot = LineChart(this.plotContext[channel])

        val linedatasetDewpoint = LineDataSet(null, "Dew Point")
        val linedatasetSetpoint = LineDataSet(null, "Set Point")
        val linedatasetTemperature = LineDataSet(null, "Temperature")

        val data = LineData()
        data.addDataSet(linedatasetDewpoint)
        data.addDataSet(linedatasetSetpoint)
        data.addDataSet(linedatasetTemperature)

        plot.data = data

        // look and feel:
        plot.description.text = "Channel " + (channel + 1).toString()
        //mpandroidchart axis values, legend and description text color does not support dark/night mode
        // so we can avoid the issue forcing a color suitable both for light and dark modes...
        plot.axisLeft.textColor=Color.GRAY
        plot.axisRight.textColor=Color.GRAY
        plot.legend.textColor=Color.GRAY
        plot.description.textColor=Color.GRAY
        //plot.axisRight.textColor=getResources().getColor(R.color.red)
        linedatasetDewpoint.setDrawCircles(false)
        linedatasetSetpoint.setDrawCircles(false)
        linedatasetTemperature.setDrawCircles(false)
        linedatasetDewpoint.setDrawValues(false)
        linedatasetSetpoint.setDrawValues(false)
        linedatasetTemperature.setDrawValues(false)
        //Inspect code: "Use of setter method instead of property access syntax
        //linedatasetDewpoint.setColor(Color.BLUE)
        linedatasetDewpoint.color=Color.BLUE
        //Inspect code: "Use of setter method instead of property access syntax
        //linedatasetSetpoint.setColor(Color.GREEN)
        linedatasetSetpoint.color=Color.GREEN
        //Inspect code: "Use of setter method instead of property access syntax
        //linedatasetTemperature.setColor(Color.RED)
        linedatasetTemperature.color=Color.RED
        linedatasetDewpoint.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        linedatasetSetpoint.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        linedatasetTemperature.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        linedatasetDewpoint.setDrawHighlightIndicators(false)
        linedatasetSetpoint.setDrawHighlightIndicators(false)
        linedatasetTemperature.setDrawHighlightIndicators(false)
        plot.xAxis.setDrawLabels(false)
        plot.xAxis.setDrawGridLines(false)

        // Populate the PlotData singleton
        this.plotChart[channel] = plot
        this.index_DewPoint[channel] = plot.data.getIndexOfDataSet(linedatasetDewpoint)
        this.index_SetPoint[channel] = plot.data.getIndexOfDataSet(linedatasetSetpoint)
        this.index_Temperature[channel] = plot.data.getIndexOfDataSet(linedatasetTemperature)
        this.timestamptInit[channel] = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        this.nextVal[channel] = 0f

    }

    fun clearPlot(channel: Int) {
        /*
        I did not find a way to "reset" the plot, so finally I choose
        clear the plot, destroy it and make it again.
        This function is for clear the plot.
        I shall do any notes about my attempts about "reset" the plot.
         */

        /* This delete all the data without delete the datasets
        for(i in 0 until this.plotChart[channel]?.data?.dataSetCount!!)
            this.plotChart[channel]?.data?.dataSets?.get(i)?.clear()
         */
        /* This delete all the data, but also the datasets
        this.plotChart[channel]?.data?.clearValues()
         */
        /* This delete all the data, but also the datasets and the data object
        this.plotChart[channel]?.clear()
         */
        /* The most difficult thing is "reset" the grid and axis ranges after rest the data.
        I don't not found the way to do this... :(
         */

        // Clear data, datasets and data objects:
        this.plotChart[channel]?.clear()
        this.plotChart[channel]?.notifyDataSetChanged()

        // Reset status values of the singleton
        this.index_DewPoint[channel] = null
        this.index_SetPoint[channel] = null
        this.index_Temperature[channel] = null
        this.nextVal[channel] = 0f
        this.timestamptInit[channel] = null
        // note: context only change in create or resume activity

        /* just AFTER call this routine you should:
        1- detach the plot from the layout
        2- set the plot to null
        3- Make a new plot by call makePlot
        4- Launch plot by call launchPlot
         */
        // hope set plot to null get the garbage collector to free resources
        //this.plotChart[channel]?.invalidate()
    }

    fun pauseMark(channel: Int) {
        val mark = LimitLine(this.nextVal[channel])
        mark.lineColor = Color.GRAY

        this.plotChart[channel]?.xAxis?.addLimitLine(mark)
    }

    fun savePlot(channel: Int){
        if(nextVal[channel] == 0f){
            Toast.makeText(this.plotContext[channel], "PLOT EMPTY: NOT SAVED", Toast.LENGTH_LONG).show()
            return
        }
        if(this.plotContext[channel]?.let {
                PermissionChecker.checkSelfPermission(
                    it,
                    "android.permission.WRITE_EXTERNAL_STORAGE"
                )
            } == PackageManager.PERMISSION_GRANTED){
            val myDirName = "/ArduheaterBT"
            val timestampNow: String = SimpleDateFormat("HHmmss").format(Date())
            val fileName = "channel_${channel+1}-${timestamptInit[channel]}-${timestampNow}"
            val description = "ArduheaterBT plot for channel ${channel+1} between ${timestamptInit[channel]} and next $timestampNow"
            if(true == plotChart[channel]?.saveToGallery(fileName, myDirName, description, Bitmap.CompressFormat.PNG, 100))
                Toast.makeText(this.plotContext[channel], "PLOT SAVED TO GALLERY", Toast.LENGTH_LONG).show()
            else Toast.makeText(this.plotContext[channel], "ERROR SAVING PLOT", Toast.LENGTH_LONG).show()
        }
        else{
            Toast.makeText(this.plotContext[channel], "NO WRITE PERMISSION", Toast.LENGTH_LONG).show()
        }
    }

}