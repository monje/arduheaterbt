package android.example.arduheaterbt

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.example.arduheaterbt.databinding.ActivityMainBinding
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var myDevice: BluetoothDevice
    private lateinit var serviceIntent: Intent

    private lateinit var binding: ActivityMainBinding
    private val ACTIVITY_SELECT_PAIRED_DEVICE = 0

    private val adapter by lazy { PagerAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.heaterdata = HeaterData

        serviceIntent = Intent(applicationContext, ArduheaterbtService::class.java)
        HeaterData.menuActivity = this

        //////Pager for fragment and tabs
        val pager: ViewPager2 = findViewById(R.id.pager)
        pager.adapter=adapter
        /// TAB.:
        // Inspect code: "Redundant SAM-constructor"
        //val tabLayoutMediator = TabLayoutMediator(binding.tablayout, pager,
        //        TabLayoutMediator.TabConfigurationStrategy { tab, position ->
        val tabLayoutMediator = TabLayoutMediator(binding.tablayout, pager
        ) { tab, position ->
            // In general, you can use here a when switch for the value of getInt("ARGUMENT")
            // but in this case is much easier
            /*
            when(position) {
                0 -> {
                    tab.text = "CHANNEL 1"
                }
                1 -> {
                    tab.text = "CHANNEL 2"
                }
                2 -> {
                    tab.text = "CHANNEL 3"
                }
                3 -> {
                    tab.text = "CHANNEL 4"
                }
            }
            */
            tab.text = "OUT ${position + 1}"
        }
        tabLayoutMediator.attach()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.mainmenu, menu)
        menu.findItem(R.id.connect).title = HeaterData.CONNECT_TEXT
        return super.onPrepareOptionsMenu(menu)
        //return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.connect -> {
                if (HeaterData.CONNECT_TEXT != HeaterData.DEFAULT_CONNECT_TEXT) {
                    disconnectDialog()
                    true
                } else {
                    val intent = Intent(this, SelectPairedDevice::class.java)
                    startActivityForResult(intent, ACTIVITY_SELECT_PAIRED_DEVICE)
                    true
                }
            }
            R.id.about -> {
                infoDialog(getString(R.string.about))
                true
            }
            R.id.notes -> {
                infoDialog(getString(R.string.notes))
                true
            }
            R.id.version -> {
                val versionName : String = BuildConfig.VERSION_NAME
                val toDisplay: String = getString(R.string.version).format(versionName)
                infoDialog(toDisplay)
                // Next should be for a "static" info screen
                //infoDialog(getString(R.string.version))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //https://www.journaldev.com/309/android-alert-dialog-using-kotlin
    private fun disconnectDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Confirm Disconnect:")

        builder.setPositiveButton("DISCONNECT") { _, _ ->
            stopService(serviceIntent)
        }

        /*builder.setNegativeButton("CANCEL") { dialog, which ->
            Toast.makeText(applicationContext, "CANCEL PRESSED", Toast.LENGTH_SHORT).show()
        }*/

        // Cancel button at left, for coherence with other 3-choice dialogs
        builder.setNeutralButton("CANCEL") { dialog, which ->
        }
        builder.show()
    }

    private fun infoDialog(content: String) {
        val builder = AlertDialog.Builder(this)
        val webView = WebView(builder.context)
        webView.loadData(content, "text/html", "utf-8")
        builder.setView(webView)

        builder.setPositiveButton("ACCEPT") { _, _ ->
        }

        /*builder.setNegativeButton("CANCEL") { _, _ ->
        }

        builder.setNeutralButton("Maybe") { dialog, which ->
        Toast.makeText(applicationContext, "Maybe", Toast.LENGTH_SHORT).show()
        }*/
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            ACTIVITY_SELECT_PAIRED_DEVICE -> {
                if (resultCode == Activity.RESULT_OK) {
                    this.myDevice = data!!.getParcelableExtra("myDev")!!
                    serviceIntent.putExtra("myDev", this.myDevice)
                    applicationContext.startService(serviceIntent)
                } else {
                    Toast.makeText(applicationContext, "NO OK", Toast.LENGTH_LONG).show()
                }
            }
            else -> {
                Toast.makeText(applicationContext, "NO REQUEST CODE", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun onClickShowSet(view: View) {
        val intent = Intent(this, ShowSetActivity::class.java)
        startActivity(intent)
    }

}
