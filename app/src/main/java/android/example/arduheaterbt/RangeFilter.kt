package android.example.arduheaterbt

import android.text.InputFilter
import android.text.Spanned
import androidx.core.text.isDigitsOnly

// SEE: https://android--code.blogspot.com/2020/08/android-kotlin-edittext-limit-number.html
// Use also with correct
class RangeFilter(min: Float, max: Float): InputFilter {
    private val mini: Float = min
    private val maxi: Float = max
    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val replacement: String = source?.subSequence(start, end).toString()
        val newVal: String = dest.toString().substring(0, dstart) + replacement + dest.toString()
            .substring(dend, dest.toString().length)
        if (!replacement.isDigitsOnly()) return null
        val value: Float
        //Inspect code: "Assignment should be lifted out of try"
        return try {
            value = newVal.toFloat()
            //Inspect code: "Assignment should be lifted out of if"
            if ((value >= this.mini) && (value <= this.maxi)) null
            else ""
        } catch (e: NumberFormatException) {
            null
        }
    }
}