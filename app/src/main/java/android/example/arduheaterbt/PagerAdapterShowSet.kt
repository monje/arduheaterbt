package android.example.arduheaterbt

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerAdapterShowSet(fa: FragmentActivity): FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = 4

    override fun createFragment(position: Int): Fragment {
        val fragment = ShowSetFragment()
        fragment.arguments = Bundle().apply {
            putInt("ARGUMENT", position)
        }
        return fragment
    }
}